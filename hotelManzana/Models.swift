//
//  RoomType.swift
//  hotelManzana
//
//  Created by umar on 10/21/17.
//  Copyright © 2017 iOS. All rights reserved.
//

// dibuat file sendiri , karena sebetulnya roomType sudah tidak ada andil dengan addRegistration 
// error ketika pemanggilan di class SelectRoomType -> tidak terdeteksi, karena deklarasi class RoomType ada di dalam class AddRegistration
// pelajarannya adalah , static itu dipanggil dari nama  class nya , jadi ketika asumsi pertama seharusnya pemanggilannya adalah pamanggilan dari 2 class (class AddREgister sebagai parent class , dan class RoomType sebagai sub class yang menyimpan array yang akan di tampilkan di class SelectRoom)


import Foundation

struct RoomType {
    var id: Int
    var name: String
    var shortname: String
    var price: Int
    
    init(id: Int, name: String, shortname: String, price: Int) {
        self.id = id
        self.name = name
        self.shortname = shortname
        self.price = price
    }
    
    static var all: [RoomType] {
        return [RoomType(id: 0, name: "Two Queens", shortname: "2Q", price: 179),
                RoomType(id: 1, name: "One King", shortname: "K", price: 209),
                RoomType(id: 2, name: "PentHouse Suite", shortname: "PHS", price: 309)
        ]
    }
    
}

struct Registration {
    var firstName: String
    var lastName: String
    var email: String
    var checkInDate: Date
    var checkOutDate: Date
    var numberOfAdult: Int
    var numberOfChildren: Int
    var roomType: RoomType
    var WiFi: Bool
    
    init(firstName: String, lastName: String, email: String, checkInDate: Date, checkOutDate: Date, numberOfAdult: Int, numberOfChildren: Int, roomType: RoomType, WiFi: Bool ) {
        self.firstName = firstName
        self.lastName = lastName
        self.checkInDate = checkInDate
        self.checkOutDate = checkOutDate
        self.email = email
        self.numberOfAdult = numberOfAdult
        self.numberOfChildren = numberOfChildren
        self.roomType = roomType
        self.WiFi = WiFi
    }
    
}

extension RoomType: Equatable {
    static func ==(lhs: RoomType, rhs: RoomType) -> Bool {
        return lhs.id  == rhs.id
    }
}


